[https://johanark.gitlab.io/spacegame/](https://johanark.gitlab.io/spacegame/)

# Controls and Keybindings

## MOVEMENT

*(White Player)*  

W - Accelerate forward  
A - Rotate counter-clockwise    
S - Accelerate backward  
D - Rotate clockwise  

*(Red Player)*  

Up Arrow - Accelerate forward  
Left Arrow - Rotate counter-clockwise  
Down Arrow - Accelerate backward  
Right Arrow - Rotate clockwise  
