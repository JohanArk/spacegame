/**
 * Created by johan.arkbo on 2019-12-19.
 */


// creates the canvas
var gameArea =
{
    canvas : document.createElement("canvas"),

    start : function()
    {
        this.canvas.width = document.body.clientWidth;
        this.canvas.height = document.body.clientHeight;
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        this.interval = setInterval(updateGameArea, 20);
    },

    update : function ()
    {
        this.canvas.width = document.body.clientWidth;
        this.canvas.height = document.body.clientHeight;

        updateGameArea();
    },

    clear : function()
    {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
};

gameArea.start();

var isDebug = false;

function Star(width, height, x, y)
{
    this.width = width;
    this.height = height;

    this.x = x;
    this.y = y;

    this.color = "#FFF";

    this.update = function ()
    {
        var r = Math.floor(Math.random() * 10000);


        if (r == 6)
        {
            this.twinkling();
        }
        else
        {
            this.color = "#FFF";
        }

        ctx = gameArea.context;
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x, this.y, this.width, this.height);

    };

    this.twinkling = function ()
    {
        this.color = "transparent";
    }
}

function Player(name, width, height, color, x, y)
{
    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;
    ctx = gameArea.context;
    ctx.fillStyle = color;
    ctx.fillRect(this.x, this.y, this.width, this.height);

    this.rotation = 0;
    this.degrees = 0;
    this.acceleration = 0;
    
    this.isRotate = false;
    this.isAccelerate = false;

    this.direction = { x: 0, y: 0 };

    this.update = function()
    {
        // Moves player
        this.x += this.direction.x;
        this.y += this.direction.y;

        if (this.isAccelerate)
        {
            this.direction.x += (Math.cos(this.rotation * Math.PI / 180) / 10) * this.acceleration;
            this.direction.y += (Math.sin(this.rotation * Math.PI / 180) / 10) * this.acceleration;
        }

        if (this.isRotate)
        {
            this.rotation += this.degrees;
        }

        // if center of square is beyond left side
        if (this.x + this.width/2 < 0)
        {
            this.x += gameArea.canvas.width;
        }
        // if center of square is beyond right side
        if (this.x + this.width/2 > gameArea.canvas.width)
        {
            this.x -= gameArea.canvas.width;
        }
        // if center of square is beyond top side
        if (this.y + this.height/2 < 0)
        {
            this.y += gameArea.canvas.height;
        }
        // if center of square is beyond bottom side
        if (this.y + this.height/2 > gameArea.canvas.height)
        {
            this.y -= gameArea.canvas.height;
        }


        ctx = gameArea.context;

        ctx.save();

        ctx.translate(this.x + this.width/2, this.y + this.height/2);

        ctx.rotate(this.rotation * Math.PI / 180);

        ctx.fillStyle = color;

        ctx.fillRect(-(this.width/2), -(this.height/2), this.width, this.height);

        ctx.fillStyle = "black";
        ctx.fillRect(-(this.width * 0.9/2), -(this.height * 0.9/2), this.width * 0.9, this.height * 0.9);

        ctx.fillStyle = "green";
        ctx.fillRect(-(this.width/2) + this.width, -(this.height/2) + this.height/2 - 2.5, 5, 5);

        ctx.restore();
    };

    this.rotate = function (degrees)
    {
        this.degrees = degrees;
    };
    
    this.move = function (speed)
    {
        this.acceleration = speed;
    }
}

var players =
    [
        new Player("X-wing", 30, 30, "white", 10, 120),
        new Player("X-wing", 30, 30, "red", 120, 120)
    ];



var stars =
[
    // new Star(2.5, 2.5, 100, 100)
];

function initStars()
{
    for (var i = 0; i <= 500; i++)
    {

        var x = Math.floor(Math.random() * document.body.clientWidth);
        var y = Math.floor(Math.random() * document.body.clientHeight);

        // Small star
        var size = Math.floor(Math.random() * 2) + 1;

        var bigStar = Math.floor(Math.random() * 15) + 1;

        if (bigStar == 4)
        {
            size = bigStar;
        }

        stars[i] = new Star(size, size, x, y);
    }
}

initStars();

function updateGameArea()
{
    gameArea.clear();

    // Updating stars
    for (star of stars)
    {
        star.update();
    }

    // Updating players
    for (player of players)
    {
        player.update();
    }
}

