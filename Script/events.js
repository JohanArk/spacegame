/**
 * Created by johan on 2019-12-22.
 */

// key released event
window.addEventListener("keyup", function ()
{
    const e = event;

    // key: W, Stop acceleration
    if ( e.keyCode == 87 )
    {
        players[0].isAccelerate = false;
    }

    // key: S, Stop acceleration
    if ( e.keyCode == 83 )
    {
        players[0].isAccelerate = false;
    }

    // key: A, Stop rotation
    if ( e.keyCode == 65 )
    {
        players[0].isRotate = false;
    }

    // key: D, Stop rotation
    if ( e.keyCode == 68 )
    {
        players[0].isRotate = false;
    }

    // key: Arrow-Up, Stop acceleration
    if (e.keyCode == 38)
    {
        players[1].isAccelerate = false;
    }

    // key: Arrow-Down, Stop acceleration
    if (e.keyCode == 40)
    {
        players[1].isAccelerate = false;
    }

    // key: Arrow-Left, Stop rotation
    if (e.keyCode == 37)
    {
        players[1].isRotate = false;
    }

    // key: Arrow-Right, Stop rotation
    if (e.keyCode == 39)
    {
        players[1].isRotate = false;
    }
});

// Key pressed event
window.addEventListener( "keydown", function ()
{
    const e = event;

    if (isDebug)
    {
        console.log("%cPressed Key %c" + e.keyCode, "color: orange; font-weight: bold;", "color: lightblue");
    }

    // key: W, Accelerate
    if ( e.keyCode == 87 )
    {
        players[0].move(2);
        players[0].isAccelerate = true;
    }

    // key: S, De-accelerate
    if ( e.keyCode == 83 )
    {
        players[0].move(-2);
        players[0].isAccelerate = true;
    }

    // key: A,  Rotate counter clockwise
    if ( e.keyCode == 65 )
    {
        players[0].rotate(-6);
        players[0].isRotate = true;
    }

    // key: D, Rotate clockwise
    if ( e.keyCode == 68 )
    {
        players[0].rotate(6);
        players[0].isRotate = true;
    }

    // key: Space, Attack
    if ( e.keyCode == 32 )
    {
        console.log("%cAttack", "color: red;");
    }

    // key: F1, Enable/Disable debugger
    if (e.keyCode == 112)
    {
        isDebug = isDebug != true;
        console.log("%cDebugger " + isDebug, "color: yellow;");
    }

    // key: Arrow-Up, Accelerate
    if (e.keyCode == 38)
    {
        players[1].move(2);
        players[1].isAccelerate = true;
    }

    // key: Arrow-Down, De-accelerate
    if (e.keyCode == 40)
    {
        players[1].move(-2);
        players[1].isAccelerate = true;
    }

    // key: Arrow-Left, Rotate counter clockwise
    if (e.keyCode == 37)
    {
        players[1].rotate(-6);
        players[1].isRotate = true;
    }

    // key: Arrow-Right, Rotate clockwise
    if (e.keyCode == 39)
    {
        players[1].rotate(6);
        players[1].isRotate = true;
    }
});

// resize event
window.addEventListener("resize", function ()
{
    if (isDebug)
    {
        console.log("%cBrowser Resize", "color: yellow; font-weight: bold;");
    }

    initStars();
    gameArea.update();
});

// mouse move on canvas event
document.getElementsByTagName("canvas")[0].addEventListener("mousemove", function(event)
{
    if (isDebug)
    {
        const {x, y} = event;

        console.log(`%cMouse X: %c${x} %cY: %c${y}`, "color: gray;", "color: lightgreen;", "color: gray;", "color: lightgreen;");
    }
});

